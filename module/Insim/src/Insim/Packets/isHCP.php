<?php

namespace Insim\Packets;

class isHCP extends Packet {

    const PACK = 'CCCx';
    const UNPACK = 'CSize/CType/CReqI/CZero';

    protected $Size = 68;       # 68
    protected $Type = Packet::ISP_HCP;  # ISP_HCP
    public $ReqI;               # 0
    protected $Zero = null;
    public $Info = array();     # H_Mass and H_TRes for each car : XF GTI = 0 / XR GT = 1 etc

    public function unpack($rawPacket) {
        parent::unpack($rawPacket);

        for ($i = 0; $i < 32; ++$i) {
            $this->Info[$i] = new CarHCP(substr($rawPacket, 4 + ($i * 2), 2));
        }

        return $this;
    }

    public function pack() {
        $return = '';

        foreach ($this->Info as $carHCP) {
            $return .= $carHCP->pack();
        }

        unset($this->Info);

        return parent::pack() . $return;
    }

}

class CarHCP extends Packet {

    const PACK = 'CC';
    const UNPACK = 'CH_Mass/CH_TRes';

    public $H_Mass;  # 0 to 200 - added mass (kg)
    public $H_TRes;  # 0 to  50 - intake restriction

}
