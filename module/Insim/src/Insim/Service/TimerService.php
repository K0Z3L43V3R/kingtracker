<?php

namespace Insim\Service;

use Application\Service\CoreService;
use Insim\Model\Timer;

class TimerService extends CoreService {

    protected $timer_sec_last = 0;
    public $timers = [];

    public function __construct() {
        
    }

    public function addTimer(Timer $timer) {
        $this->timers[$timer->uid] = $timer;
    }

    public function removeTimer($uuid = null) {
        if ($uuid) {
            unset($this->timers[$uuid]);
        } else {
            $this->timers = [];
        }
    }

    /**
     * Get timer by name
     * @param string | array $name
     * @return boolean
     */
    public function getTimerByName($name) {
        if (is_array($name)) {
            foreach ($this->timers as $tm) {
                if (in_array($tm->name, $name)) {
                    return $tm;
                }
            }
        } else {
            foreach ($this->timers as $tm) {
                if ($tm->name == $name) {
                    return $tm;
                }
            }
        }

        return false;
    }

    public function run() {
        if (time() - $this->timer_sec_last >= 1) {
            if ($this->timers) {
                foreach ($this->timers as $timer) {
                    $timer->tick();

                    if ($timer->state == Timer::TIMER_FINISHED) {
                        unset($this->timers[$timer->uid]);
                    }
                }
            }
            
            $this->timer_sec_last = time();
        }
    }

}
