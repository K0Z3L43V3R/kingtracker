<?php

namespace Insim\Model;

use Application\Model\CoreModel;

/**
 * Parameter object
 */
class Param extends CoreModel{
    const MASTER_SERVER = 'MASTER_SERVER';
    
    public $id;
    public $_group;
    public $name;
    public $value;
    public $type;
    public $params;
    public $readonly;

    public function exchangeArray($data) {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->_group = (!empty($data['_group'])) ? $data['_group'] : null;
        $this->name = (!empty($data['name'])) ? $data['name'] : null;
        $this->value = (!empty($data['value'])) ? $data['value'] : null;
        $this->type = (!empty($data['type'])) ? $data['type'] : null;
        $this->params = (!empty($data['params'])) ? $data['params'] : null;
        $this->readonly = (!empty($data['readonly'])) ? $data['readonly'] : null;
    }

    public function exchangeObject() {
        $data = array();
        $data['id'] = (!empty($this->id)) ? $this->id : null;
        $data['_group'] = (!empty($this->_group)) ? $this->_group : null;
        $data['name'] = (!empty($this->name)) ? $this->name : null;
        $data['value'] = (!empty($this->value)) ? $this->value : null;
        $data['type'] = (!empty($this->type)) ? $this->type : null;
        $data['params'] = (!empty($this->params)) ? $this->params : null;
        $data['readonly'] = (!empty($this->readonly)) ? $this->readonly : null;
        
        return $data;
    }
}