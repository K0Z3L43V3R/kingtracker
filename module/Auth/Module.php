<?php

namespace Auth;

use Auth\Adapter\SaltedCallbackCheckAdapter as AuthAdapter;
use Auth\Controller\IndexController;
use Auth\Model\User;
use Zend\Authentication\AuthenticationService;
use Zend\Crypt\Password\Bcrypt;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Mvc\Controller\ControllerManager;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface {

    public function onBootstrap(MvcEvent $e) {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        //$this->bootstrapSession($e);
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Auth\Service\UserService' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new User());
                    $tableGateway = new TableGateway('users', $dbAdapter, null, $resultSetPrototype);

                    $service = new Service\UserService($tableGateway);
                    $service->setConfig($sm->get('Config'));
                    return $service;
                },
                'AuthService' => function ($sm) {
                    $adapter = $sm->get('Zend\Db\Adapter\Adapter');

                    $credentialValidationCallback = function($identity, $requestCredential) use ($sm) {
                                $config = $sm->get('Config');
                                $bcrypt_cost = $config['bcrypt_cost'];

                                if (!$identity['active'])
                                    return false;

                                $bcrypt = new Bcrypt();
                                $bcrypt->setCost($bcrypt_cost);
                                $bcrypt->setSalt($identity['salt']);
                                return $bcrypt->verify($requestCredential, $identity['passwd']);
                            };
                    $authAdapter = new AuthAdapter($adapter, 'users', 'login', 'passwd', $credentialValidationCallback);

                    $auth = new AuthenticationService();
                    $auth->setAdapter($authAdapter);
                    return $auth;
                },
            )
        );
    }
    
    public function getControllerConfig() {
        return array(
            'factories' => array(
                'Auth\Controller\Index' => function(ControllerManager $cm) {
                    $sm = $cm->getServiceLocator();
                    $c = new IndexController();
                    $c->setUserService($sm->get('Auth\Service\UserService'));
                    $c->setAuthService($sm->get('AuthService'));
                    return $c;
                },
            ),
        );
    }
}
