<?php

namespace Insim\UI\Elements;

use Insim\Model\PlayerClass;
use Insim\Packets\isBFN;
use Insim\Packets\isBTC;
use Insim\Packets\isBTN;
use Insim\Types\ButtonStyles;
use Zend\Debug\Debug;

class Table extends Element {

    public $current;
    public $values;
    public $values_count = 0;
    public $button_id_value = 0;
    public $key_value = -1;
    public $width;
    public $columns = [];
    public $rows = [];
    public $current_page = 1;
    public $rows_per_page = 15;
    public $status_line = '';
    public $max_page;
    public $button_content_min;
    public $button_content_max;

    function __construct(PlayerClass $player, $label = '', $height = 5, $top = 0, $left = 0) {
        parent::__construct($player, $label, $height, $top, $left);

        // columns
        $this->columns = array(
            'track' => array('width' => 20, 'name' => 'Track', 'style' => ButtonStyles::ISB_LEFT),
            'layout' => array('width' => 50, 'name' => 'Track', 'style' => ButtonStyles::ISB_LEFT),
            'race' => array('width' => 20, 'name' => 'Race length', 'style' => ButtonStyles::ISB_LEFT),
        );
    }

    function show($id_current, $top = 0, $left = 0) {
        parent::show($id_current, $top, $left);

        $this->setValues();

        $this->button_content_min = $id_current + 1;

        // Lines
        $button = new isBTN();
        $newLine = 0;
        foreach ($this->rows as $key => $row) {
            $rowIndex = 0;
            foreach ($this->columns as $keyCol => $column) {
                $button->ReqI = ++$id_current;
                $button->ClickID = $button->ReqI;
                $button->L = !$rowIndex ? ($this->left) + 1 : $button->L + $button->W;
                $button->T = $this->top + 2 + 12 + $newLine;
                $button->H = 5;
                $button->W = $column['width'];
                $button->BStyle = ButtonStyles::ISB_CLICK + ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY + (isset($column['style']) ? $column['style'] : 0);
                $button->Text = isset($row[$keyCol]) ? $row[$keyCol] : '';

                $this->send($button);
                $this->rows_buttons[$key][$keyCol] = $button->ClickID;
                $rowIndex++;
            }
            $newLine += 5;
        }

        $this->button_content_max = $id_current;
        $this->end_id = $id_current;
    }

    function hide() {
        $bfn = new isBFN();
        $bfn->SubT = isBFN::BFN_DEL_BTN;
        $bfn->UCID = $this->player->UCID;
        $bfn->ClickID = $this->button_content_min;
        $bfn->ClickMax = $this->button_content_max;

        $this->player->host->insim->send($bfn);

        parent::hide();
    }

    function setValues() {
        $this->rows = array();

        foreach ([1, 2, 3, 4, 5, 6, 7] as $value) {
            $this->rows[] = array(
                'track' => "^3!{$value}",
                'layout' => "^3!{$value}",
                'race' => "^3!{$value}",
            );
        }

        for ($i = count($this->rows); $i < $this->rows_per_page; $i++) {
            $this->rows[$i] = array(
                'track' => "",
                'layout' => "",
                'race' => "",
            );
        }

        $this->status_line = 'status line';
    }

    public function eventClick(isBTC $packet) {
        parent::eventClick($packet);
    }

}
