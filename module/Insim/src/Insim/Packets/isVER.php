<?php

namespace Insim\Packets;

use Insim\Types\InSimTypes;

/**
 * Version packet
 */
class isVER extends Packet {

    const PACK = 'CCCxa8a6v';
    const UNPACK = 'CSize/CType/CReqI/CZero/a8Version/a6Product/vInSimVer';

    protected $Size = 20;               # 20
    protected $Type = Packet::ISP_VER;          # ISP_VERSION
    public $ReqI;                       # ReqI as received in the request packet
    protected $Zero;
    public $Version;                    # LFS version, e.g. 0.3G
    public $Product;                    # Product : DEMO or S1
    public $InSimVer = InSimTypes::INSIM_VERSION;   # InSim Version : increased when InSim packets change

}
