<?php

namespace Insim\Types;

class PenaltyReason {

    const PENR_UNKNOWN = 0;         // unknown or cleared penalty
    const PENR_ADMIN = 1;           // penalty given by admin
    const PENR_WRONG_WAY = 2;       // wrong way driving
    const PENR_FALSE_START = 3;     // starting before green light
    const PENR_SPEEDING = 4;        // speeding in pit lane
    const PENR_STOP_SHORT = 5;      // stop-go pit stop too short
    const PENR_STOP_LATE = 6;       // compulsory stop is too late

}