<?php

namespace Admin\Controller;

use Admin\Form\MasterServerForm;
use Admin\Form\MasterServerInputFilter;
use Auth\Controller\AdminController;
use Insim\Service\ParamsService;
use Insim\Service\TaskService;
use Zend\Debug\Debug;
use Zend\View\Model\ViewModel;

class SettingsController extends AdminController {

    protected $paramsService;
    protected $taskService;

    public function __construct() {
        
    }

    public function masterServerAction() {
        $view = new ViewModel();
        
        $params = array();
        
        foreach($this->paramsService->fetchAllBy(array('_group' => \Insim\Model\Param::MASTER_SERVER), true) as $param){
            $params[$param->name] = $param;
        }
        
        $form = new MasterServerForm();
        $request = $this->getRequest();

        if ($request->isPost()) {
            $post = $request->getPost();

            $inputFilter = new MasterServerInputFilter();
            $form->setInputFilter($inputFilter->inputFilter);
            $form->setData($post);
            if ($form->isValid()) {
                $data = $form->getData();

                foreach($params as $key => $param){
                    if(isset($data[$key])){
                        $param->value = $data[$key];
                        $this->paramsService->save($param);
                    }
                }

                $this->flashMessenger()->setNamespace('success')->addMessage('Your changes have been saved');
                return $this->redirect()->toRoute('admin/default', array('controller' => 'settings', 'action' => 'masterserver'));
            }

            $view->setVariable('post', $post);
        }

        return $view->setVariables(array(
                    'form' => $form,
                    'params' => $params
        ));
    }

    public function setTaskService(TaskService $serice) {
        $this->taskService = $serice;
    }
    public function setParamsService(ParamsService $serice) {
        $this->paramsService = $serice;
    }
}
