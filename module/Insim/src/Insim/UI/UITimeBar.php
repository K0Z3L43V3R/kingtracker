<?php

namespace Insim\UI;

use Insim\Helper\InSimHelper;
use Insim\Helper\Math;
use Insim\Model\PlayerClass;
use Insim\Model\Timer;
use Insim\Packets\isBTN;
use Insim\Packets\isLap;
use Insim\Packets\isSPX;
use Insim\Service\TimerService;
use Insim\Types\ButtonStyles;
use Insim\Types\MsgTypes;
use Insim\Types\SpeedUnit;

/**
 * Time bar
 */
class UITimeBar extends UI {

    private $buttons;
    private $timerService;

    function __construct($alias, PlayerClass &$player) {
        $id_start = 1;
        foreach ($player->ui->ui_list_perm as $key => $ui) {
            if ($ui->displayed) {
                $id_start = $ui->id_end + 1;
            }
        }

        $id_end = $id_start + 15;

        $this->width = 87;
        $this->height = 6;
        $this->top = 0;
        $this->left = (200 - $this->width) / 2;

        $this->buttons = array(
            'event' => array('id' => 0, 'value' => 'L1', 'width' => 10, 'color' => MsgTypes::WHITE, 'style' => ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY),
            'split' => array('id' => 0, 'value' => '', 'width' => 15, 'color' => MsgTypes::YELLOW, 'style' => ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY),
            'split_diff' => array('id' => 0, 'value' => '', 'width' => 10, 'color' => '', 'style' => ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY),
            'sector' => array('id' => 0, 'value' => '', 'width' => 15, 'color' => MsgTypes::YELLOW, 'style' => ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY),
            'sector_diff' => array('id' => 0, 'value' => '', 'width' => 10, 'color' => '', 'style' => ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY),
            'lap' => array('id' => 0, 'value' => '', 'width' => 15, 'color' => MsgTypes::LIGHT_BLUE, 'style' => ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY),
            'speed' => array('id' => 0, 'value' => '', 'width' => 10, 'color' => MsgTypes::LIGHT_BLUE, 'style' => ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY),
        );

        $this->buttons_more = array(
            'pos' => array('id' => 0, 'value' => '', 'width' => 10, 'color' => MsgTypes::WHITE, 'style' => ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY),
            'tb' => array('id' => 0, 'value' => '', 'width' => 15, 'color' => MsgTypes::WHITE, 'style' => ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY),
            'tb_splits' => array('id' => 0, 'value' => '', 'width' => 35, 'color' => MsgTypes::WHITE, 'style' => ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY),
            'pb' => array('id' => 0, 'value' => '', 'width' => 15, 'color' => MsgTypes::WHITE, 'style' => ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY),
            'top_speed' => array('id' => 0, 'value' => '', 'width' => 10, 'color' => MsgTypes::WHITE, 'style' => ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY),
        );

        $this->timerService = new TimerService();

        parent::__construct($alias, $player, ($id_start), $id_end);
    }

    public function update() {
        $this->timerService->run();
    }

    public function show() {
        $button = new isBTN();
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left;
        $button->T = $this->top;
        $button->H = $this->height; //+ 4;
        $button->W = $this->width;

        $button->BStyle = ButtonStyles::ISB_DARK;
        $button->Text = '';

        $this->send($button);

        // Set initial data
        if (isset($this->player->lapsService->stint)) {
            $stint = $this->player->lapsService->stint;
            $this->buttons['event']['value'] = "L{$stint->currentLap}";
            if ($stint->bestLap > 0) {
                $this->buttons['lap']['value'] = InSimHelper::timeToString($stint->bestLap);
            }
        }


        $left = 1;
        foreach ($this->buttons as $buttonID => $btn) {
            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L = $this->left + $left;
            $button->T = $this->top;
            $button->H = 5;
            $button->W = $btn['width'];

            $button->BStyle = $btn['style'];
            $button->Text = $btn['value'];

            $this->buttons[$buttonID]['id'] = $button->ReqI;
            $this->send($button);

            $left += $button->W;
        }

        /* $left = 1;
          foreach ($this->buttons_more as $buttonID => $btn) {
          $button->ReqI = ++$this->id_current;
          $button->ClickID = $button->ReqI;
          $button->L = $this->left + $left;
          $button->T = $this->top + 5;
          $button->H = 4;
          $button->W = $btn['width'];

          $button->BStyle = $btn['style'];
          $button->Text = $btn['value'];

          $this->buttons_more[$buttonID]['id'] = $button->ReqI;
          $this->send($button);

          $left += $button->W;
          } */


        $this->button_content_max = $this->id_current;
        $this->end = $this->button_content_max;
        
        parent::show();
    }

    public function redraw($faded = false) {
        if (!$this->displayed)
            return;

        $button = new isBTN();

        foreach ($this->buttons as $key => $btn) {
            $button->ReqI = $btn['id'];
            $button->ClickID = $btn['id'];
            $button->Text = (!$faded ? $btn['color'] : '') . $btn['value'];
            $this->send($button);
        }
        
        /* foreach ($this->buttons_more as $key => $btn) {
          $button->ReqI = $btn['id'];
          $button->ClickID = $btn['id'];
          $button->Text = (!$faded ? $btn['color'] : '') . $btn['value'];
          $this->send($button);
          } */
    }

    public function eventSplit(isSPX $split) {
        if ($split->STime == 3600000 OR !isset($this->player->compCar))
            return;

        $stint = $this->player->lapsService->stint;
        /* $best_splits = $stint->bestSplits;
          foreach($best_splits as $key => $value){
          $best_splits[$key] = InSimHelper::timeToString($value);
          } */

        $this->buttons['event']['value'] = "L{$stint->currentLap}S{$split->Split}";
        $this->buttons['split']['value'] = InSimHelper::timeToString($split->STime);
        $this->buttons['sector']['value'] = InSimHelper::timeToString($stint->sectors[$split->Split]);
        if ($this->player->settings->get('unit_speed') == SpeedUnit::KPH) {
            $this->buttons['speed']['value'] = Math::speedToKph($this->player->compCar->Speed, 2);
        } else {
            $this->buttons['speed']['value'] = Math::speedToMph($this->player->compCar->Speed, 2);
        }

        $negative = false;
        $this->buttons['split_diff']['value'] = InSimHelper::diffToString($stint->split_diff, $negative);
        $this->buttons['split_diff']['color'] = !$negative && $stint->split_diff != 0 ? MsgTypes::RED : MsgTypes::GREEN;

        $negative = false;
        $this->buttons['sector_diff']['value'] = InSimHelper::diffToString($stint->sector_diff, $negative);
        $this->buttons['sector_diff']['color'] = !$negative && $stint->sector_diff != 0 ? MsgTypes::RED : MsgTypes::GREEN;

        //$this->buttons_more['tb_splits']['value'] = implode(' - ', $best_splits);

        $this->redraw();

        $this->timerService->addTimer(new Timer('fadeOut', 10, function() {
            $this->redraw(true);
        }));
    }

    public function eventLap(isLap $lap) {
        if ($lap->LTime == 3600000 OR !isset($this->player->compCar->Speed))
            return;

        $stint = $this->player->lapsService->stint;

        $this->buttons['event']['value'] = "L{$stint->currentLap}";
        $this->buttons['split']['value'] = InSimHelper::timeToString($lap->LTime);
        $this->buttons['sector']['value'] = InSimHelper::timeToString($stint->sectors[$stint->finish_sector]);

        $this->buttons['lap']['value'] = InSimHelper::timeToString($stint->bestLap);

        if ($this->player->settings->get('unit_speed') == SpeedUnit::KPH) {
            $this->buttons['speed']['value'] = Math::speedToKph($this->player->compCar->Speed, 2);
        } else {
            $this->buttons['speed']['value'] = Math::speedToMph($this->player->compCar->Speed, 2);
        }

        $negative = false;
        $this->buttons['split_diff']['value'] = InSimHelper::diffToString($stint->lap_diff, $negative);
        $this->buttons['split_diff']['color'] = !$negative && $stint->lap_diff != 0 ? MsgTypes::RED : MsgTypes::GREEN;

        $negative = false;
        $this->buttons['sector_diff']['value'] = InSimHelper::diffToString($stint->sector_diff, $negative);
        $this->buttons['sector_diff']['color'] = !$negative && $stint->sector_diff != 0 ? MsgTypes::RED : MsgTypes::GREEN;


        $this->redraw();

        $this->timerService->addTimer(new Timer('fadeOut', 10, function() {
            $this->redraw(true);
        }));
    }

    public function eventRaceStarted() {
        $stint = $this->player->lapsService->stint;

        // Reset values
        foreach ($this->buttons as $key => $btn) {
            $this->buttons[$key]['value'] = '';
        }

        $this->buttons['event']['value'] = "L{$stint->currentLap}";
        if (isset($stint->bestLap)) {
            $this->buttons['lap']['value'] = InSimHelper::timeToString($stint->bestLap);
        }

        $this->redraw();

        $this->timerService->addTimer(new Timer('fadeOut', 10, function() {
            $this->redraw(true);
        }));
    }

    public function eventCarReset() {
        $stint = $this->player->lapsService->stint;

        // Reset values
        foreach ($this->buttons as $key => $btn) {
            $this->buttons[$key]['value'] = '';
        }

        $this->buttons['event']['value'] = "L{$stint->currentLap}";
        if ($stint->bestLap > 0) {
            $this->buttons['lap']['value'] = InSimHelper::timeToString($stint->bestLap);
        }

        $this->redraw();

        $this->timerService->addTimer(new Timer('fadeOut', 10, function() {
            $this->redraw(true);
        }));
    }

    public function eventTrackChanged() {
        $stint = $this->player->lapsService->stint;

        // Reset values
        foreach ($this->buttons as $key => $btn) {
            $this->buttons[$key]['value'] = '';
        }

        $this->buttons['event']['value'] = "L{$stint->currentLap}";
        if ($stint->bestLap > 0) {
            $this->buttons['lap']['value'] = InSimHelper::timeToString($stint->bestLap);
        }

        $this->redraw();

        $this->timerService->addTimer(new Timer('fadeOut', 10, function() {
            $this->redraw(true);
        }));
    }

}
