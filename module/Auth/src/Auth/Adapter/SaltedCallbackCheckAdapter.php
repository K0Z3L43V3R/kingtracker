<?php

namespace Auth\Adapter;

use Exception;
use Zend\Authentication\Adapter\DbTable\CallbackCheckAdapter;
use Zend\Authentication\Result as AuthenticationResult;
use Zend\Db\Adapter\Adapter as DbAdapter;

class SaltedCallbackCheckAdapter extends CallbackCheckAdapter {

    public function __construct(
    DbAdapter $zendDb, $tableName = null, $identityColumn = null, $credentialColumn = null, $credentialValidationCallback = null
    ) {
        parent::__construct($zendDb, $tableName, $identityColumn, $credentialColumn, $credentialValidationCallback);
    }

    /**
     * _authenticateValidateResult() - This method attempts to validate that
     * the record in the resultset is indeed a record that matched the
     * identity provided to this adapter.
     * MOD: pass whole identity to callback function
     *
     * @param  array $resultIdentity
     * @return AuthenticationResult
     */
    protected function authenticateValidateResult($resultIdentity) {
        try {
            // $callbackResult = call_user_func($this->credentialValidationCallback, $resultIdentity[$this->credentialColumn], $this->credential);
            $callbackResult = call_user_func($this->credentialValidationCallback, $resultIdentity, $this->credential);
        } catch (Exception $e) {
            $this->authenticateResultInfo['code'] = AuthenticationResult::FAILURE_UNCATEGORIZED;
            $this->authenticateResultInfo['messages'][] = $e->getMessage();
            return $this->authenticateCreateAuthResult();
        }
        if ($callbackResult !== true) {
            $this->authenticateResultInfo['code'] = AuthenticationResult::FAILURE_CREDENTIAL_INVALID;
            $this->authenticateResultInfo['messages'][] = 'Supplied credential is invalid.';
            return $this->authenticateCreateAuthResult();
        }

        $this->resultRow = $resultIdentity;

        $this->authenticateResultInfo['code'] = AuthenticationResult::SUCCESS;
        $this->authenticateResultInfo['messages'][] = 'Authentication successful.';
        return $this->authenticateCreateAuthResult();
    }

}
