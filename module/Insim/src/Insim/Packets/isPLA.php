<?php

namespace Insim\Packets;

/**
 * Pit LAne
 */
class isPLA extends Packet {

    const PACK = 'CCxCCxxx';
    const UNPACK = 'CSize/CType/CReqI/CPLID/CFact/CSp1/CSp2/CSp3';

    protected $Size = 8;        # 8
    protected $Type = self::ISP_PLA;  # ISP_PLA
    protected $ReqI;            # 0
    public $PLID;               # player's unique id
    public $Fact;               # pit lane fact (see below)
    protected $Sp1;
    protected $Sp2;
    protected $Sp3;

}
