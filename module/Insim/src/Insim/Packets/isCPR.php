<?php

namespace Insim\Packets;

/**
 * Player rename
 */
class isCPR extends Packet {

    const PACK = 'CCxCa24a8';
    const UNPACK = 'CSize/CType/CReqI/CUCID/a24PName/A8Plate';

    protected $Size = 36;       # 36
    protected $Type = Packet::ISP_CPR;  # ISP_CPR
    public $ReqI = null;        # 0
    public $UCID;               # unique id of the connection
    public $PName;              # new name
    public $Plate;              # number plate - NO ZERO AT END!

}
