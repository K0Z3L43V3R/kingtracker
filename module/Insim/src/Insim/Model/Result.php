<?php

namespace Insim\Model;

use Application\Model\CoreModel;

class Result extends CoreModel{
    const TYPE_RACE = 1;
    const TASK_QUALIFY = 2;
    
    public $id;
    public $host_id;
    public $type;
    public $info;
    public $finished;
    public $time;

    public $results;
    public $count;

    public function exchangeArray($data) {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->host_id = (!empty($data['host_id'])) ? $data['host_id'] : null;
        $this->type = (!empty($data['type'])) ? $data['type'] : null;
        $this->info = (!empty($data['info'])) ? json_decode($data['info'], true) : null;
        $this->finished = isset($data['finished']) ? $data['finished'] : 0;
        $this->time = (!empty($data['time'])) ? $data['time'] : null;
        
        $this->count = (!empty($data['count'])) ? $data['count'] : null;
    }

    public function exchangeObject() {
        $data = array();
        $data['id'] = (!empty($this->id)) ? $this->id : null;
        $data['host_id'] = (!empty($this->host_id)) ? $this->host_id : null;
        $data['type'] = (!empty($this->type)) ? $this->type : null;
        $data['info'] = (!empty($this->info)) ? json_encode($this->info) : null;
        $data['finished'] = isset($this->finished) ? $this->finished : 0;
        $data['time'] = (!empty($this->time)) ? $this->time : null;
        return $data;
    }

}