<?php

namespace Insim\Service;

use Application\Service\CoreService;
use Zend\Db\TableGateway\TableGateway;

class TimeCompareService extends CoreService {

    public $player;
    public $LFSWService;

    public function __construct(TableGateway $tableGateway) {
        parent::__construct($tableGateway);
        $this->debug = true;
    }

    public function newPlayerEvent(){
        //\Zend\Debug\Debug::dump('Time compare new player');
    }
    
    public function eventTrackChanged(){
        //\Zend\Debug\Debug::dump('Time compare track change');
        
    }

}
