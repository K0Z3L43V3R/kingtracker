<?php

namespace Insim\UI;

use Insim\Model\PlayerClass;
use Insim\Packets\isBFN;
use Insim\Packets\isBTC;
use Insim\Packets\isBTN;
use Insim\Packets\isBTT;
use Insim\Types\ButtonStyles;

class UI {

    protected $player;
    public $displayed = 0;
    public $current_page = 1;
    public $max_page = 1;
    public $id_start;
    public $id_current;
    public $id_end;
    public $width;
    public $height;
    public $left;
    public $top;
    public $title = '';
    public $alias = '';
    public $breadcrumbs = '';
    public $status_line = '';
    public $button_id_close = 0;
    public $button_id_help = 0;
    public $button_id_left = 0;
    public $button_id_right = 0;
    public $button_id_start = 0;
    public $button_id_end = 0;
    public $button_id_status_line = 0;
    public $button_id_title = 0;
    public $show_help = false;
    public $popupVisible = false;
    public $button_content_min = 0;
    public $button_content_max = 0;
    public $popup_min = 0;
    public $popup_current = 0;
    public $popup_max = 0;
    public $help_lines = array();

    const COL_ENABLED = '^6';
    const COL_DISABLED = '^1';
    const COL_NEUTRAL = '^8';

    public function __construct($alias, PlayerClass &$player, $id_start = 100, $id_end = 254) {
        $this->player = $player;
        $this->alias = $alias;

        $this->id_start = $id_start;
        $this->id_end = $id_end;
        $this->id_current = $this->id_start;

        $bfn = new isBFN();
        $bfn->SubT = isBFN::BFN_DEL_BTN;
        $bfn->UCID = $this->player->UCID;
        $bfn->ClickID = $this->id_start;
        $bfn->ClickMax = $this->id_end;
        
        $this->player->host->insim->send($bfn);
    }

    public function show() {
        $this->displayed = 1;
        $this->popupVisible = 0;
    }

    public function showBase() {
        $this->id_current = $this->id_start;

        $button = new isBTN();
        $button->ReqI = $this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left;
        $button->T = $this->top;
        $button->H = $this->height + 8;
        $button->W = $this->width;
        $button->BStyle = ButtonStyles::ISB_LIGHT;
        $button->Text = '';

        $this->send($button);

        // Header
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left + 1;
        $button->T = $this->top + 1;
        $button->H = 12;
        $button->W = $this->width - 2;
        $button->BStyle = ButtonStyles::ISB_DARK;
        $button->Text = '';

        $this->send($button);

        // Header 2nd bg
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left + 1;
        $button->T = $this->top + 1;
        $button->H = 12;
        $button->W = $this->width - 2 - ($this->show_help ? 14 : 7);
        $button->BStyle = ButtonStyles::ISB_DARK;
        $button->Text = '';

        $this->send($button);

        // Help
        if ($this->show_help) {
            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L += $button->W;
            $button->T = $this->top + 1;
            $button->H = 12;
            $button->W = 7;
            $button->BStyle = ButtonStyles::ISB_DARK;
            $button->Text = '';

            $this->send($button);

            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            //$button->L -= 1;
            $button->T = $this->top + 1;
            $button->H = 11;
            $button->W = 7;
            $button->BStyle = ButtonStyles::ISB_CLICK + ButtonStyles::COLOUR_LIGHT_GREY;
            $button->Text = '?';

            $this->button_id_help = $this->id_current;
            $this->send($button);
        }

        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L += $button->W;
        $button->T = $this->top + 1;
        $button->H = 12;
        $button->W = 7;
        $button->BStyle = ButtonStyles::ISB_DARK;
        $button->Text = '';

        $this->send($button);

        // close button
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        //$button->L = ;
        $button->T = $this->top - 1;
        $button->H = 14;
        $button->W = 7;
        $button->BStyle = ButtonStyles::ISB_CLICK + ButtonStyles::COLOUR_LIGHT_GREY;
        $button->Text = 'x';

        $this->button_id_close = $this->id_current;
        $this->send($button);

        // Title
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left + 1;
        $button->T = $this->top + 2;
        $button->H = 7;
        $button->W = $this->width - 4;
        $button->BStyle = ButtonStyles::ISB_LEFT + ButtonStyles::COLOUR_LIGHT_GREY;
        $button->Text = '^7' . $this->title;
        $this->button_id_title = $button->ClickID;

        $this->send($button);
    }

    public function showBaseSmall($bg = true) {
        $this->id_current = $this->id_start;

        $button = new isBTN();
        $button->ReqI = $this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left;
        $button->T = $this->top;
        $button->H = $this->height + 8;
        $button->W = $this->width;
        $button->BStyle = ButtonStyles::ISB_LIGHT;
        $button->Text = '';

        $this->send($button);

        // Header
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left + 1;
        $button->T = $this->top + 1;
        $button->H = 5;
        $button->W = $this->width - 2;
        $button->BStyle = ButtonStyles::ISB_DARK;
        $button->Text = '';

        $this->send($button);

        // Header 2nd bg
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left + 1;
        $button->T = $this->top + 1;
        $button->H = 5;
        $button->W = $this->width - 2 - ($this->show_help ? 14 : 7);
        $button->BStyle = ButtonStyles::ISB_DARK;
        $button->Text = '';

        $this->send($button);

        // Help
        if ($this->show_help) {
            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L += $button->W;
            $button->T = $this->top + 1;
            $button->H = 12;
            $button->W = 7;
            $button->BStyle = ButtonStyles::ISB_DARK;
            $button->Text = '';

            $this->send($button);

            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            //$button->L -= 1;
            $button->T = $this->top + 1;
            $button->H = 11;
            $button->W = 7;
            $button->BStyle = ButtonStyles::ISB_CLICK + ButtonStyles::COLOUR_LIGHT_GREY;
            $button->Text = '?';

            $this->button_id_help = $this->id_current;
            $this->send($button);
        }

        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L += $button->W;
        $button->T = $this->top + 1;
        $button->H = 5;
        $button->W = 7;
        $button->BStyle = ButtonStyles::ISB_DARK;
        $button->Text = '';

        $this->send($button);

        // close button
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        //$button->L = ;
        $button->T = $this->top - 1;
        $button->H = 8;
        $button->W = 7;
        $button->BStyle = ButtonStyles::ISB_CLICK + ButtonStyles::COLOUR_LIGHT_GREY;
        $button->Text = 'x';

        $this->button_id_close = $this->id_current;
        $this->send($button);

        // Title
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left + 1;
        $button->T = $this->top + 1;
        $button->H = 5;
        $button->W = $this->width - 4;
        $button->BStyle = ButtonStyles::ISB_LEFT + ButtonStyles::COLOUR_LIGHT_GREY;
        $button->Text = '^7' . $this->title;

        $this->send($button);

        if ($bg) {
            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L = $this->left + 1;
            $button->T = $this->top + 2 + 5;
            $button->H = $this->height;
            $button->W = $this->width - 2;
            $button->BStyle = ButtonStyles::ISB_DARK;
            $button->Text = '';

            $this->send($button);
        }
    }

    public function showFooter($topOffset = 0) {
        $button = new isBTN();
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left + 1;
        $button->T = $this->top + 3 + 12 + 90 + $topOffset;
        $button->H = 5;
        $button->W = $this->width - 2;
        $button->BStyle = ButtonStyles::ISB_DARK;
        $button->Text = '';
        $this->send($button);

        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left + 1;
        $button->H = 5;
        $button->W = 10;
        $button->BStyle = ButtonStyles::ISB_DARK + ButtonStyles::ISB_CLICK;
        $button->Text = '<<<';
        $this->send($button);
        $this->button_id_start = $button->ClickID;

        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L += $button->W;
        $button->H = 5;
        $button->W = 10;
        $button->BStyle = ButtonStyles::ISB_DARK + ButtonStyles::ISB_CLICK;
        $button->Text = '<';
        $this->send($button);
        $this->button_id_left = $button->ClickID;

        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L += $button->W;
        $button->H = 5;
        $button->W = $this->width - 2 - 40;
        $button->BStyle = ButtonStyles::ISB_CLICK;
        $button->Text = $this->status_line;
        $button->TypeIn = 10;
        $this->send($button);
        $this->button_id_status_line = $button->ClickID;
        $button->TypeIn = false;

        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L += $button->W;
        $button->H = 5;
        $button->W = 10;
        $button->BStyle = ButtonStyles::ISB_DARK + ButtonStyles::ISB_CLICK;
        $button->Text = '>';
        $this->send($button);
        $this->button_id_right = $button->ClickID;

        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L += $button->W;
        $button->H = 5;
        $button->W = 10;
        $button->BStyle = ButtonStyles::ISB_DARK + ButtonStyles::ISB_CLICK;
        $button->Text = '>>>';
        $this->send($button);
        $this->button_id_end = $button->ClickID;
        
        $this->id_current++;
    }

    public function checkID($id) {
        return ($id >= $this->id_start && $id <= $this->id_end) ? true : false;
    }

    public function send($packet) {
        $packet->UCID = $this->player->UCID;
        //$this->player->host->insim->send($packet);
        $this->player->host->insim->sendQued(clone $packet, $this->player);
        //usleep(100);
        //echo PHP_EOL.'Button: '.$packet->ClickID;
        //Debug::dump($packet);
    }

    public function eventClick(isBTC $packet) {
        switch ($packet->ClickID) {
            case $this->button_id_close:
                $this->close();
                break;
            case $this->button_id_help:
                $this->help();
                break;
            case $this->button_id_left:
                $this->left();
                break;
            case $this->button_id_right:
                $this->right();
                break;
            case $this->button_id_start:
                $this->start();
                break;
            case $this->button_id_end:
                $this->end();
                break;
            case $this->button_id_status_line:
                $this->setPage();
                break;
        }
    }

    public function eventType(isBTT $packet) {
        switch ($packet->ClickID) {
            case $this->button_id_status_line:
                $this->setPage($packet);
                break;
        }
    }

    public function redrawContent() {
        
    }

    public function close() {
        $this->displayed = 0;

        $bfn = new isBFN();
        $bfn->SubT = isBFN::BFN_DEL_BTN;
        $bfn->UCID = $this->player->UCID;
        $bfn->ClickID = $this->id_start;
        $bfn->ClickMax = $this->id_end;
        
        $this->player->host->insim->send($bfn);
        
        if(isset($this->player->ui->ui_list[$this->alias]))
            unset($this->player->ui->ui_list[$this->alias]);
    }

    public function left() {
        if ($this->current_page <= 1)
            return;

        $this->current_page = $this->current_page - 1;
        $this->redrawContent();
    }

    public function right() {
        if ($this->current_page >= $this->max_page)
            return;

        $this->current_page = $this->current_page + 1;
        $this->redrawContent();
    }

    public function start() {
        if ($this->current_page <= 1)
            return;

        $this->current_page = 1;
        $this->redrawContent();
    }

    public function end() {
        if ($this->current_page >= $this->max_page)
            return;

        $this->current_page = $this->max_page;
        $this->redrawContent();
    }

    public function setPage(isBTT $packet) {
        $page = intval(trim($packet->Text));

        if ($page <= 0)
            $page = 1;
        if ($page >= $this->max_page)
            $page = $this->max_page;

        $this->current_page = $page;
        $this->redrawContent();
    }

    public function help() {
        if (!$this->show_help) {
            return;
        }

        $this->popupVisible = !$this->popupVisible;
        $this->displayed = !$this->popupVisible;

        if ($this->popupVisible) {
            $this->show_popup();
        } else {
            $this->hide_popup();
        }
    }

    public function show_popup() {
        // Clear content buttons
        $bfn = new isBFN();
        $bfn->SubT = isBFN::BFN_DEL_BTN;
        $bfn->UCID = $this->player->UCID;
        $bfn->ClickID = $this->button_content_min;
        $bfn->ClickMax = $this->button_content_max;

        $this->player->host->insim->send($bfn);

        // Show popup buttons
        // Reset first button ID
        $this->id_current = $this->button_content_min;
        $this->popup_min = $this->id_current;

        // Filter bg
        $button = new isBTN();
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left + 1;
        $button->T = $this->top + 2 + 12;
        $button->H = 95;
        $button->W = $this->width - 2;
        $button->BStyle = ButtonStyles::ISB_DARK;
        $button->Text = '';

        $this->send($button);

        $height = 0;
        foreach ($this->help_lines as $line) {
            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L = $this->left + 3;
            $button->T = $this->top + 2 + 15 + $height;
            $button->H = 4;
            $button->W = $this->width - 2;
            $button->BStyle = ButtonStyles::ISB_LEFT;
            $button->Text = $line;

            $this->send($button);
            $height += $button->H;
        }
    }

    public function hide_popup() {
        // Reset first button ID
        $this->id_current = $this->button_content_min;

        // Hide all popup buttons
        $bfn = new isBFN();
        $bfn->SubT = isBFN::BFN_DEL_BTN;
        $bfn->UCID = $this->player->UCID;
        $bfn->ClickID = $this->popup_min;
        $bfn->ClickMax = $this->popup_max;

        $this->player->host->insim->send($bfn);

        // Restore content
        $this->show(false);
    }
    
    public function update(){
        
    }

}
