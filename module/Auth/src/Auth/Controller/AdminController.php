<?php

namespace Auth\Controller;

use Zend\Debug\Debug;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;

class AdminController extends AbstractActionController{
    public $user;
    public $auth;

    public function setEventManager(EventManagerInterface $events) {
        parent::setEventManager($events);
        $events->attach(MvcEvent::EVENT_DISPATCH, array($this, 'beforeDispatch'), 100);
    }

    public function beforeDispatch(MvcEvent $event) {
        if (!$this->auth->hasIdentity()) {
            return $this->redirect()->toRoute('auth/default', array('controller' => 'Index', 'action' => 'login'));
        }
        
        $this->layout()->setVariable('user', $this->auth->getIdentity());
    }

    public function setAuthService(\Zend\Authentication\AuthenticationService $service) {
        $this->auth = $service;
    }

}
