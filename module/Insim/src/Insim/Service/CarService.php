<?php

namespace Insim\Service;

use Application\Service\CoreService;
use Zend\Db\TableGateway\TableGateway;

class CarService extends CoreService {
    protected $cars;

    public function __construct(TableGateway $tableGateway) {
        parent::__construct($tableGateway);
    }

    public function getOfficial(){
        
        foreach($this->fetchAllBy(array('custom' => 0), true, false, 'ord ASC') as $car){
            switch ($car->licence){
                case 'DEMO':
                    $this->cars['DEMO'][$car->code] = $car;
                    break;
                case 'S1':
                    $this->cars['S1'][$car->code] = $car;
                    break;
                case 'S2':
                    $this->cars['S2'][$car->code] = $car;
                    break;
            }
        }
        
        return $this->cars;
    }
}
