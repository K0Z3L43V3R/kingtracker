<?php
namespace Insim\Filter;

/**
 * This function takes the last comma or dot (if any) to make a clean float, 
 * ignoring thousand separator, currency or any other letter
 * 
 * $num = '1.999,369€'; // float(1999.369)
 * $num = '126,564,789.33 m²'; // float(126564789.33)
 */
class FloatFilter extends \Zend\Filter\AbstractFilter {

    public function filter($value) {
        $dotPos = strrpos($value, '.');
        $commaPos = strrpos($value, ',');
        $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos : 
            ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);

        if (!$sep) {
            return floatval(preg_replace("/[^0-9]/", "", $value));
        } 

        return floatval(
            preg_replace("/[^0-9]/", "", substr($value, 0, $sep)) . '.' .
            preg_replace("/[^0-9]/", "", substr($value, $sep+1, strlen($value)))
        );
    }
}