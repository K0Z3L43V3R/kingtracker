<?php

use Insim\Packets\Packet;

namespace Insim\Packets;

/**
 * General purpose 4 byte packet
 */
class isSMALL extends Packet {

    const PACK = 'CCCCV';
    const UNPACK = 'CSize/CType/CReqI/CSubT/VUVal';
    const SMALL_NONE = 0;  //  0					: not used
    const SMALL_SSP = 1;  //  1 - instruction		: start sending positions
    const SMALL_SSG = 2;  //  2 - instruction		: start sending gauges
    const SMALL_VTA = 3;  //  3 - report			: vote action
    const SMALL_TMS = 4;  //  4 - instruction		: time stop
    const SMALL_STP = 5;  //  5 - instruction		: time step
    const SMALL_RTP = 6;  //  6 - info			: race time packet (reply to GTH)
    const SMALL_NLI = 7;  //  7 - instruction		: set node lap interval
    const SMALL_ALC = 8;  //  8 - both ways		: set or get allowed cars (TINY_ALC)
    const VOTE_NONE = 0;   // 0 - no vote
    const VOTE_END = 1;   // 1 - end race
    const VOTE_RESTART = 2; // 2 - restart
    const VOTE_QUALIFY = 3;  // 3 - qualify

    protected $Size = 8;            # always 8
    protected $Type = Packet::ISP_SMALL;    # always ISP_SMALL
    public $ReqI;                   # 0 unless it is an info request or a reply to an info request
    public $SubT;                   # subtype, from SMALL_ enumeration (e.g. SMALL_SSP)
    public $UVal;
    private static $allowedCars = array(
        1 => 'XFG',
        2 => 'XRG',
        4 => 'XRT',
        8 => 'RB4',
        16 => 'FXO',
        32 => 'LX4',
        64 => 'LX6',
        128 => 'MRT',
        256 => 'UF1',
        512 => 'RAC',
        1024 => 'FZ5',
        2048 => 'FOX',
        4096 => 'XFR',
        8192 => 'UFR',
        16384 => 'FO8',
        32768 => 'FXR',
        65536 => 'XRR',
        131072 => 'FZR',
        262144 => 'BF1',
        524288 => 'FBM',
    );

    public static function parseAllowedCars($UVal) {
        $cars = array();

        foreach (self::$allowedCars as $key => $car) {
            if (parent::binaryMask($key, $UVal)) {
                $cars[] = $car;
            }
        }

        return $cars;
    }

}
