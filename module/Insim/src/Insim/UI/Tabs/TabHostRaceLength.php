<?php

namespace Insim\UI\Tabs;

use Insim\Model\PlayerClass;
use Insim\Types\MsgTypes;
use Insim\UI\Elements\BtnInput;
use Insim\UI\Elements\BtnLabel;
use Insim\UI\Elements\BtnSwitch;

class TabHostRaceLength extends Tab {

    function __construct(PlayerClass &$player) {
        parent::__construct($player);

        $this->buttons = array(
            'race-pits' => new BtnInput($player, 'Desired race length in minutes:', 5),
            'table' => new \Insim\UI\Elements\Table($player, 'Race / Qualify length overrides:', 5, $top = 5, $left = 0)
        );
    }

    function setData($data = array()) {
        parent::setData($data);

        $this->buttons['race-pits']->setValues(array('race-pits' => MsgTypes::WHITE . $this->player->host->settings->get('race-pits')));
    }
    
    function hide() {
        parent::hide();
    }

}
