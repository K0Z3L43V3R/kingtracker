<?php

namespace Insim\Types;

use ReflectionClass;

class LFSLicence {

    const DEMO = 'Demo';
    const S1 = 'S1';
    const S2 = 'S2';
    const S3 = 'S3';

    public static function getAll() {
        $rf = new ReflectionClass(self::class);
        return $rf->getConstants();
    }

}
