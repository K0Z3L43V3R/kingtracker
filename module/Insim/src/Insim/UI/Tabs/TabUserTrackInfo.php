<?php

namespace Insim\UI\Tabs;

use Insim\Model\PlayerClass;
use Insim\Types\MsgTypes;
use Insim\UI\Elements\BtnSwitch;

class TabUserTrackInfo extends Tab {

    function __construct(PlayerClass &$player) {
        parent::__construct($player);

        $this->buttons = array(
            'trinfo_show' => new BtnSwitch($player, $this->player->translator->translateLFS('SHOW_TRACK_INFO'), 5)
        );
        
        $this->buttons['trinfo_show']->eventValueChanged = function($value){
            $this->player->settings->update('trinfo_show', $value);
            
            if($value){
                $this->player->ui->showUITrackInfo();
            }else{
                $this->player->ui->hideUITrackInfo();
            }
        };
    }

    function setData($data = array()) {
        parent::setData($data);
        
        $valuesYesNo = array(
            '0' => MsgTypes::RED.$this->player->translator->translateLFS('NO'),
            '1' => MsgTypes::GREEN.$this->player->translator->translateLFS('YES'),
        );
        
        $this->buttons['trinfo_show']->setValues($valuesYesNo, @$this->player->settings->get('trinfo_show'));
    }

}
