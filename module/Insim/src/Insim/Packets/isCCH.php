<?php

namespace Insim\Packets;

/**
 * Camera change
 */
class isCCH extends Packet {

    const PACK = 'CCxCCxxx';
    const UNPACK = 'CSize/CType/CReqI/CPLID/CCamera/CSp1/CSp2/CSp3';

    protected $Size = 8;        # 8
    protected $Type = self::ISP_CCH;  # ISP_CCH
    protected $ReqI;            # 0
    public $PLID;               # player's unique id
    public $Camera;             # view identifier (see below)
    protected $Sp1;
    protected $Sp2;
    protected $Sp3;

}
