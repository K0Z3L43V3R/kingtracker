<?php

namespace Insim\Packets;

/**
 * Button function
 */
class isAXI extends Packet {

    const PACK = 'CCCxCCva32';
    const UNPACK = 'CSize/CType/CReqI/CZero/CAXStart/CNumCP/vNumO/a32LName';

    protected $Size = 40;       # 40
    protected $Type = Packet::ISP_AXI;  # ISP_AXI
    public $ReqI;               # 0 unless this is a reply to an TINY_AXI request
    protected $Zero;
    public $AXStart;            # autocross start position
    public $NumCP;              # number of checkpoints
    public $NumO;               # number of objects
    public $LName;              # the name of the layout last loaded (if loaded locally)

}
