<?php

namespace Insim\Packets;

/**
 * Message
 */
class isIII extends Packet {

    const PACK = 'CCxxCCxxa64';
    const UNPACK = 'CSize/CType/CReqI/CZero/CUCID/CPLID/CSp2/CSp3/a*Msg';

    protected $Size;            # Variable size
    protected $Type = Packet::ISP_III;  # ISP_III
    protected $ReqI = 0;        # 0
    protected $Zero = null;
    public $UCID = 0;           # connection's unique id (0 = host)
    public $PLID = 0;           # player's unique id (if zero, use UCID)
    protected $Sp2 = null;
    protected $Sp3 = null;
    public $Msg;

}
