<?php

namespace Insim\Packets;

/**
 * Car contact packet
 */
class isCON extends Packet {

    const PACK = 'CCCCvv';
    const UNPACK = 'CSize/CType/CReqI/CZero/vSpClose/vTime';

    protected $Size = 40;       # 40
    protected $Type = Packet::ISP_CON;  # ISP_CON
    protected $ReqI = 0;        # 0
    protected $Zero;
    public $SpClose;            # high 4 bits : reserved / low 12 bits : closing speed (10 = 1 m/s)
    public $Time;               # looping time stamp (hundredths - time since reset - like TINY_GTH)
    public $A = array();
    public $B = array();

    public function unpack($rawPacket) {
        parent::unpack($rawPacket);

        $this->A = new CarContact(substr($rawPacket, 8, 16));
        $this->B = new CarContact(substr($rawPacket, 24, 16));

        return $this;
    }

}

class CarContact extends Packet{

    const PACK = 'CCCcCCCCCCccss';
    const UNPACK = 'CPLID/CInfo/CSp2/cSteer/CThrBrk/CCluHan/CGearSp/CSpeed/CDirection/CHeading/cAccelF/cAccelR/sX/sY';

    public $PLID;
    public $Info;       # like Info byte in CompCar (CCI_BLUE / CCI_YELLOW / CCI_LAG)
    public $Sp2;        # spare
    public $Steer;      # front wheel steer in degrees (right positive)
    public $ThrBrk;     # high 4 bits : throttle    / low 4 bits : brake (0 to 15)
    public $CluHan;     # high 4 bits : clutch      / low 4 bits : handbrake (0 to 15)
    public $GearSp;     # high 4 bits : gear (15=R) / low 4 bits : spare
    public $Speed;      # m/s
    public $Direction;  # car's motion if Speed > 0 : 0 = world y direction, 128 = 180 deg
    public $Heading;    # direction of forward axis : 0 = world y direction, 128 = 180 deg
    public $AccelF;     # m/s^2 longitudinal acceleration (forward positive)
    public $AccelR;     # m/s^2 lateral acceleration (right positive)
    public $X;          # position (1 metre = 16)
    public $Y;          # position (1 metre = 16)

}
