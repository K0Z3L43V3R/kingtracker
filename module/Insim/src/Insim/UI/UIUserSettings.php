<?php

namespace Insim\UI;

use Insim\Model\PlayerClass;
use Insim\Packets\isBTC;
use Insim\Packets\isBTN;
use Insim\Packets\isBTT;
use Insim\Types\ButtonStyles;
use Insim\UI\Tabs\TabUserNotifyGeneral;
use Insim\UI\Tabs\TabUserTimeBar;
use Insim\UI\Tabs\TabUserTrackInfo;
use Insim\UI\Tabs\TabUserUnits;

class UIUserSettings extends UI {

    public $columns = array();
    public $rows = array();
    public $cars = array();
    public $filter = array();
    public $search = array();
    public $topActions = array();
    public $own_time = array();
    public $own_buttons = array();
    public $rows_buttons = array();
    public $rows_per_page = 20;
    public $items_per_page = 20;
    public $show_help = false;
    protected $current_tab = 'ntf-general';
    protected $tabs = array();
    protected $tabTop;
    protected $tabLeft;
    protected $tabWidth;

    function __construct($alias, PlayerClass &$player, $width = 122, $height = 101, $top = 30) {
        $this->width = $width;
        $this->height = $height;
        $this->top = $top;
        $this->left = (200 - $width) / 2;

        // columns
        $this->columns = array(
            'line' => array('width' => $this->width - 2, 'name' => 'Line', 'style' => ButtonStyles::ISB_LEFT),
        );

        $this->filter = array(
            'Notifications' => array(
                'id' => 0,
                'title' => $player->translator->translateLFS('NOTIFICATIONS'),
                'values' => array(
                    'General' => array('id' => 0, 'title' => $player->translator->translateLFS('GENERAL'), 'state' => 1, 'key' => 'ntf-general'),
                )
            ),
            'User interface' => array(
                'id' => 0,
                'title' => $player->translator->translateLFS('USER_INTERFACE'),
                'values' => array(
                    //'Race control' => array('id' => 0, 'state' => 0, 'key' => 'race-control'),
                    'Time bar' => array('id' => 0, 'title' => $player->translator->translateLFS('TIME_BAR'), 'state' => 0, 'key' => 'time-bar'),
                    'Track info' => array('id' => 0, 'title' => $player->translator->translateLFS('TRACK_INFO'), 'state' => 0, 'key' => 'track-info'),
                )
            ),
            'Localization' => array(
                'id' => 0,
                'title' => $player->translator->translateLFS('LOCALIZATION'),
                'values' => array(
                    'Units' => array('id' => 0, 'title' => $player->translator->translateLFS('UNITS'), 'state' => 0, 'key' => 'loc-units'),
                )
            ),
        );

        $this->tabs = array(
            'ntf-general' => new TabUserNotifyGeneral($player),
            'track-info' => new TabUserTrackInfo($player),
            'time-bar' => new TabUserTimeBar($player),
            'loc-units' => new TabUserUnits($player),
        );

        parent::__construct($alias, $player);
    }

    public function setData() {
        $this->rows = array();

        $start = $this->current_page == 1 ? 0 : ($this->current_page - 1) * $this->rows_per_page;
        $end = $start + $this->rows_per_page - 1;

        for ($i = $start; $i <= $end; $i++) {
            $this->rows[] = array(
                'line' => isset($this->lines[$i]) ? $this->lines[$i] : '',
            );
        }

        $this->max_page = ceil(1 / $this->rows_per_page);
        $this->status_line = $this->current_page . ' / ' . $this->max_page;

        // Set tabs data
        if (isset($this->tabs[$this->current_tab])) {
            $tab = $this->tabs[$this->current_tab];
            $tab->setData();
        }
    }

    public function show($showBase = true) {
        $this->setData();

        if ($showBase)
            $this->showBase();

        $this->button_content_min = $this->id_current + 1;

        // Filter bg
        $button = new isBTN();
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left + 1;
        $button->T = $this->top + 2 + 12;
        $button->H = 87;
        $button->W = 20;
        $button->BStyle = ButtonStyles::ISB_DARK;
        $button->Text = '';

        $this->send($button);

        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left + 1 + $button->W;
        $button->T = $this->top + 2 + 12;
        $button->W = $this->width - $button->W - 2;
        $button->BStyle = ButtonStyles::ISB_DARK;
        $button->Text = '';
        $this->tabWidth = $button->W;
        $this->tabLeft = $button->L;
        $this->tabTop = $button->T;
        $this->send($button);

        // Filter
        $height = 0;
        $top = $this->top + 2 + 12;
        foreach ($this->filter as $sectionName => $filters) {
            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L = $this->left + 1;
            $button->T = $top + $height;
            $button->H = 4;
            $button->W = 20;
            $button->BStyle = ButtonStyles::ISB_LEFT + ButtonStyles::ISB_CLICK + ButtonStyles::ISB_LIGHT;
            $button->Text = '^7' . $this->filter[$sectionName]['title'];

            $height = 4;
            $top = $button->T;
            $left = $button->L;

            $this->send($button);
            $this->filter[$sectionName]['id'] = $button->ReqI;

            $fIndex = 0;
            foreach ($filters['values'] as $filterName => $filter) {
                if ($filter['state'] == -1) {
                    $color = UI::COL_DISABLED;
                } elseif ($filter['state'] == 1) {
                    $color = UI::COL_ENABLED;
                } else {
                    $color = UI::COL_NEUTRAL;
                }

                $button->ReqI = ++$this->id_current;
                $button->ClickID = $button->ReqI;
                $button->L = $left;
                $button->T = $top + $height;
                $button->H = 4;
                $button->W = 20;
                $button->BStyle = ButtonStyles::ISB_DARK + ButtonStyles::ISB_CLICK;
                $button->Text = $color . $filter['title'];

                $this->send($button);
                $this->filter[$sectionName]['values'][$filterName]['id'] = $button->ReqI;

                $top = $button->T;
                $fIndex++;
            }

            $height = 6;
            $top = $button->T;
        }

        $this->showFooter(-2);

        // Draw tab content
        if (isset($this->tabs[$this->current_tab])) {
            $tab = $this->tabs[$this->current_tab];
            $tab->show(($this->id_current), $this->tabTop, $this->tabLeft, $this->tabWidth);
        }

        $this->button_content_max = $this->id_current;

        parent::show();
    }

    public function redrawContent() {
        $this->setData();

        $button = new isBTN();
        $button->ClickID = $this->button_id_status_line;
        $button->Text = $this->status_line;
        $this->send($button);

        // Redraw tab content
        if (isset($this->tabs[$this->current_tab])) {
            $tab = $this->tabs[$this->current_tab];
            $tab->show($this->id_current, $this->tabTop, $this->tabLeft, $this->tabWidth);
        }

        parent::redrawContent();
    }

    public function eventClick(isBTC $packet) {
        parent::eventClick($packet);

        // filter click
        foreach ($this->filter as $sectionName => $filters) {
            foreach ($filters['values'] as $filterName => $filter) {
                if ($filter['id'] == $packet->ClickID) {
                    $this->setTab($packet, $sectionName, $filterName);
                    return;
                }
            }
        }

        // Tab click
        if (isset($this->tabs[$this->current_tab])) {
            $tab = $this->tabs[$this->current_tab];
            $tab->eventClick($packet);
        }
    }

    public function eventType(isBTT $packet) {
        parent::eventType($packet);
    }

    public function setTab(isBTC $packet, $sectionName, $filterName) {
        $this->current_page = 1;
        $fltr = $this->filter[$sectionName]['values'][$filterName];

        if ($this->current_tab == $fltr['key']) {
            return;
        }

        // Hide all tabs
        foreach ($this->tabs as $tab) {
            $tab->hide();
        }

        $button = new isBTN();

        // Clear all
        foreach ($this->filter as $category => $section) {
            foreach ($section['values'] as $key => $value) {
                $this->filter[$category]['values'][$key]['state'] = false;
                $button->ClickID = $value['id'];
                $button->Text = $value['title'];
                $this->send($button);
            }
        }
        
        $color = '';
        if (empty($fltr['state']) OR $fltr['state'] < 1) {
            $fltr['state'] = 1;
            $color = UI::COL_ENABLED;
        } else {
            $fltr['state'] = 0;
            $color = UI::COL_NEUTRAL;
        }

        $this->filter[$sectionName]['values'][$filterName]['state'] = $fltr['state'];
        $button->ClickID = $fltr['id'];
        $button->Text = $color . $this->filter[$sectionName]['values'][$filterName]['title'];
        $this->current_tab = $fltr['key'];

        $this->send($button);
        $this->redrawContent();
    }
    
    public function close(){
        $this->player->requestClear = false;
        parent::close();
    }

}
