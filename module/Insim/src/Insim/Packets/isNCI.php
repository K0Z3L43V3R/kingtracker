<?php

namespace Insim\Packets;

/**
 * New ConN
 */
class isNCI extends Packet {

    const PACK = 'CCCCCCCCVV';
    const UNPACK = 'CSize/CType/CReqI/CUCID/CLanguage/CSp1/CSp2/CSp3/VUserID/VIPAddress';

    protected $Size = 16;       # 16
    protected $Type = Packet::ISP_NCI;  # ISP_NCI
    public $ReqI = null;        # 0 unless this is a reply to a TINY_NCI request
    public $UCID;               # new connection's unique id (0 = host)
    public $Language;           # 1 if admin (see below)
    protected $Sp1;
    protected $Sp2;
    protected $Sp3;
    public $UserID;
    public $IPAddress;

    public function unpack($rawPacket) {
        parent::unpack($rawPacket);

        $IPSegments = [];
        for ($i = 0; $i < 4; ++$i) {
            $IPSegments[$i] = unpack("C", substr($rawPacket, 12 + $i))[1];
        }
        $this->IPAddress = implode('.', $IPSegments);

        return $this;
    }

}
