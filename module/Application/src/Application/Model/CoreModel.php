<?php

namespace Application\Model;

abstract class CoreModel {

    public abstract function exchangeArray($data);

    public abstract function exchangeObject();

    /**
     * Converts object to array
     * @return array
     */
    public function toArray() {
        return get_object_vars($this);
    }

}
