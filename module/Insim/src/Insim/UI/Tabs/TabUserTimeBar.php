<?php

namespace Insim\UI\Tabs;

use Insim\Model\PlayerClass;
use Insim\Types\MsgTypes;
use Insim\UI\Elements\BtnSwitch;

class TabUserTimeBar extends Tab {

    function __construct(PlayerClass &$player) {
        parent::__construct($player);

        $this->buttons = array(
            'timebar_show' => new BtnSwitch($player, $this->player->translator->translateLFS('SHOW_TIMEBAR'), 5),
            'timebar_reset_race' => new BtnSwitch($player, $this->player->translator->translateLFS('TIMEBAR_RESET_RACE'), 5),
            'timebar_reset_spectate' => new BtnSwitch($player, $this->player->translator->translateLFS('TIMEBAR_RESET_SPEC'), 5),
            'timebar_reset_car' => new BtnSwitch($player, $this->player->translator->translateLFS('TIMEBAR_RESET_CAR'), 5)
        );
        
        $this->buttons['timebar_show']->eventValueChanged = function($value){
            $this->player->settings->update('timebar_show', $value);
            
            if($value){
                $this->player->ui->showUITimeBar();
            }else{
                $this->player->ui->hideUITimeBar();
            }
        };
        
        $this->buttons['timebar_reset_race']->eventValueChanged = function($value){
            $this->player->settings->update('timebar_reset_race', $value);
        };
        
        $this->buttons['timebar_reset_spectate']->eventValueChanged = function($value){
            $this->player->settings->update('timebar_reset_spectate', $value);
        };
        
        $this->buttons['timebar_reset_car']->eventValueChanged = function($value){
            $this->player->settings->update('timebar_reset_car', $value);
        };
    }

    function setData($data = array()) {
        parent::setData($data);
        
        $valuesYesNo = array(
            '0' => MsgTypes::RED.$this->player->translator->translateLFS('NO'),
            '1' => MsgTypes::GREEN.$this->player->translator->translateLFS('YES'),
        );
        
        $this->buttons['timebar_show']->setValues($valuesYesNo, @$this->player->settings->get('timebar_show'));
        $this->buttons['timebar_reset_race']->setValues($valuesYesNo, @$this->player->settings->get('timebar_reset_race'));
        $this->buttons['timebar_reset_spectate']->setValues($valuesYesNo, @$this->player->settings->get('timebar_reset_spectate'));
        $this->buttons['timebar_reset_car']->setValues($valuesYesNo, @$this->player->settings->get('timebar_reset_car'));
    }

}
