<?php

namespace Admin\Service;

use Application\Service\MyService;
use Zend\Db\TableGateway\TableGateway;

class HostService extends MyService {
    public function __construct(TableGateway $tableGateway) {
        parent::__construct($tableGateway);
    }
}
