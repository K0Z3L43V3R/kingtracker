<?php

namespace Insim\Model;

use Application\Model\CoreModel;

class PlayerACL extends CoreModel {

    public $id;
    public $UName;
    public $role;

    public function exchangeArray($data) {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->UName = (!empty($data['UName'])) ? $data['UName'] : null;
        $this->role = (!empty($data['role'])) ? $data['role'] : null;
    }

    public function exchangeObject() {
        $data = array();
        $data['id'] = (!empty($this->id)) ? $this->id : null;
        $data['UName'] = (!empty($this->UName)) ? $this->UName : null;
        $data['role'] = (!empty($this->role)) ? $this->role : null;
        return $data;
    }

}
