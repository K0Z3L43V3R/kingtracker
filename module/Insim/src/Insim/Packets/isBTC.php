<?php

namespace Insim\Packets;

/**
 * Button click
 */
class isBTC extends Packet {

    const PACK = 'CCCCCCCx';
    const UNPACK = 'CSize/CType/CReqI/CUCID/CClickID/CInst/CCFlags/CSp3';
    const ISB_LMB = 1;     // left click
    const ISB_RMB = 2;     // right click
    const ISB_CTRL = 4;     // ctrl + click
    const ISB_SHIFT = 8;     // shift + click

    protected $Size = 8;        # 8
    protected $Type = Packet::ISP_BTC;  # ISP_BTC
    public $ReqI;               # ReqI as received in the IS_BTN
    public $UCID;               # connection that clicked the button (zero if local)
    public $ClickID;            # button identifier originally sent in IS_BTN
    public $Inst;               # used internally by InSim
    public $CFlags;             # button click flags - see below
    protected $Sp3;

    public function isLMB(){
        return $this->CFlags & isBTC::ISB_LMB;
    }
    
    public function isRMB(){
        return $this->CFlags & isBTC::ISB_RMB;
    }
    
    public function isCTRL(){
        return $this->CFlags & isBTC::ISB_CTRL;
    }
    
    public function isSHIFT(){
        return $this->CFlags & isBTC::ISB_SHIFT;
    }
}
