<?php

namespace Insim\Model;

use Application\Model\CoreModel;

class PlayerBan extends CoreModel {

    public $id;
    public $UName;
    public $duration;
    public $duration_date;
    public $reason;
    public $banned;
    public $banned_date;
    public $banned_by;
    public $unbanned_date;
    public $unbanned_by;

    public function exchangeArray($data) {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->UName = (!empty($data['UName'])) ? $data['UName'] : null;
        $this->duration = (!empty($data['duration'])) ? $data['duration'] : null;
        $this->duration_date = (!empty($data['duration_date'])) ? $data['duration_date'] : null;
        $this->reason = (!empty($data['reason'])) ? $data['reason'] : '';
        $this->banned = (!empty($data['banned'])) ? $data['banned'] : null;
        $this->banned_date = (!empty($data['banned_date'])) ? $data['banned_date'] : null;
        $this->banned_by = (!empty($data['banned_by'])) ? $data['banned_by'] : null;
        $this->unbanned_date = (!empty($data['unbanned_date'])) ? $data['unbanned_date'] : null;
        $this->unbanned_by = (!empty($data['unbanned_by'])) ? $data['unbanned_by'] : null;
    }

    public function exchangeObject() {
        $data = array();
        $data['id'] = (!empty($this->id)) ? $this->id : null;
        $data['UName'] = (!empty($this->UName)) ? $this->UName : null;
        $data['duration'] = (!empty($this->duration)) ? $this->duration : null;
        $data['reason'] = (!empty($this->reason)) ? $this->reason : '';
        $data['banned'] = (isset($this->banned)) ? $this->banned : 1;
        $data['banned_date'] = (!empty($this->banned_date)) ? $this->banned_date : date('Y-m-d H:i:s');
        $data['banned_by'] = (!empty($this->banned_by)) ? $this->banned_by : null;
        $data['unbanned_date'] = (!empty($this->unbanned_date)) ? $this->unbanned_date : null;
        $data['unbanned_by'] = (!empty($this->unbanned_by)) ? $this->unbanned_by : null;
        
        $now = date('Y-m-d H:i:s');
        $plus = $this->duration > 0 ? "+{$this->duration} days" : '+12 hours';
        $data['duration_date'] = date('Y-m-d H:i:s', strtotime($now . $plus));
        
        return $data;
    }
}
