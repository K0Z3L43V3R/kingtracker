<?php

namespace Insim\Service;

use Application\Service\CoreService;
use Insim\Model\Track;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

class TrackService extends CoreService {

    public $tracks;
    public $trackTypes;

    public function __construct(TableGateway $tableGateway) {
        parent::__construct($tableGateway);
        $this->debug = true;
        $this->tracks = include BASE_PATH . '/config/insim.tracks.php';
        $this->trackTypes = array(
            'asphalt', 'rally', 'reversed', 'oval', 'open'
        );
    }

    public function getByShortName($name) {
        $short_name = $name;
        $isReversed = false;
        $isOpen = false;

        $last_letter = substr($name, -1);

        switch ($last_letter) {
            case 'R':
                $isReversed = true;
                $name = substr($short_name, 0, (strlen($name) - 1));
                break;
            case 'X':
                $isOpen = true;
                $name = substr($short_name, 0, (strlen($name) - 1));
                break;
            case 'Y':
                $isOpen = true;
                $isReversed = true;
                $name = substr($short_name, 0, (strlen($name) - 1));
                break;
        }


        $track = $this->getBy(array('short_name' => $name));

        if ($track) {
            $track->isReversed = $isReversed;
            $track->isOpen = $isOpen;
            $track->name = $short_name;
            $track->circuit_number = intval(substr($track->short_name, 2));
            $track->LFSWid = $this->getLFSWid($track);

            if ($isOpen) {
                $track->lenght = '';
                $track->elevation = '';
            }
        }

        return $track;
    }

    public function getTracks($where = array()) {
        $resultSet = $this->tableGateway->select(function(Select $select) use ($where) {
            foreach ($where as $key => $value) {
                switch ($key) {
                    case 'grid':
                        if($value > 0){
                            $select->where->and->greaterThanOrEqualTo('grid', $value);
                        }
                        break;
                    case 'lenght':
                        if(isset($value['min']) && $value['min'] > 0){
                            $select->where->and->greaterThanOrEqualTo('lenght', $value['min']);
                        }
                        if(isset($value['max']) && $value['max'] > 0){
                            $select->where->and->lessThanOrEqualTo('lenght', $value['max']);
                        }
                        break;
                    case 'track':
                        $predicate = $select->where->and->NEST;
                        foreach ($value as $track) {
                            $predicate->or->equalTo($key, $track);
                        }
                        $predicate->UNNEST;
                        break;
                    case 'types':
                        $asphalt = intval($value['asphalt']);
                        $rallycross = intval($value['rallycross']);
                        $oval = intval($value['oval']);
                        
                        // Surface
                        $predicate = $select->where->and->NEST;
                        $predicateSurface = $predicate->and->NEST;
                        if($asphalt && $rallycross){
                            $predicateSurface->or->equalTo('asphalt', $asphalt);
                            $predicateSurface->or->equalTo('rallycross', $rallycross);
                            
                        }else{
                            $predicateSurface->and->equalTo('asphalt', $asphalt);
                            $predicateSurface->and->equalTo('rallycross', $rallycross);
                            
                        }
                        $predicateSurface->UNNEST;
                        
                        // Oval
                        if($oval){
                            $predicate->or->equalTo('oval', $oval);
                        }else{
                            $predicate->and->equalTo('oval', $oval);
                        }
                        
                        $predicate->UNNEST;
                        break;
                }
            }

            // No carparks and drag
            $select->where->and->equalTo('carpark', 0);
            $select->where->and->equalTo('drag', 0);
            
            //$select->order($order);
            $adapter = $this->tableGateway->getAdapter();
            //\Zend\Debug\Debug::dump($select->getSqlString($adapter->getPlatform()));
        });

        $resultSet->buffer();

        return $resultSet;
    }

    private function getLFSWid(Track $track) {
        //$trackNum = array_search($track->track, $this->tracks);

        foreach ($this->tracks as $name => $tr) {
            if ($name == $track->track) {
                return $tr['LFSW'] . ($track->circuit_number - 1) . ($track->isReversed ? 1 : 0);
            }
        }
    }
    
    public function getShortName($name){
        return isset($this->tracks[$name]) ? $this->tracks[$name]['shortName'] : false;
    }
    
}
