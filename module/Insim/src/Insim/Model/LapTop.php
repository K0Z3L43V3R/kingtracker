<?php

namespace Insim\Model;

use Application\Model\CoreModel;
use Insim\Types\PlayerFlags;
use Insim\Types\SetupFlags;

class LapTop extends CoreModel{
    public $id;
    public $player_id;
    public $track;
    public $layout;
    public $car;
    public $H_Mass;
    public $H_TRes;
    public $PFlags;
    public $PF_M;
    public $PF_W;
    public $PF_KB;
    public $PF_KBS;
    public $PF_AC;
    public $PF_AxisC;
    public $PF_BC;
    public $PF_AG;
    public $PF_HS;
    public $PF_SQ;
    public $PF_BRH;
    public $SFlags;
    public $SF_ABS;
    public $SF_TC;
    public $Tyres;
    public $clean;
    public $non_clean_reason;
    public $time;
    public $time_string;
    public $splits;
    public $sectors;
    public $added;
    public $updated;

    public $PName;

    public function exchangeArray($data) {
        $this->id = (!empty($data['id'])) ? $data['id'] : 0;
        $this->player_id = (!empty($data['player_id'])) ? $data['player_id'] : 0;
        $this->track = (!empty($data['track'])) ? $data['track'] : '';
        $this->layout = (!empty($data['layout'])) ? $data['layout'] : '';
        $this->car = (!empty($data['car'])) ? $data['car'] : '';
        $this->H_Mass = (!empty($data['H_Mass'])) ? $data['H_Mass'] : 0;
        $this->H_TRes = (!empty($data['H_TRes'])) ? $data['H_TRes'] : 0;
        $this->PFlags = (!empty($data['PFlags'])) ? $data['PFlags'] : 0;
        $this->PF_M = (!empty($data['PF_M'])) ? $data['PF_M'] : false;
        $this->PF_W = (!empty($data['PF_W'])) ? $data['PF_W'] : false;
        $this->PF_KB = (!empty($data['PF_KB'])) ? $data['PF_KB'] : false;
        $this->PF_KBS = (!empty($data['PF_KBS'])) ? $data['PF_KBS'] : false;
        $this->PF_AC = (!empty($data['PF_AC'])) ? $data['PF_AC'] : false;
        $this->PF_AxisC = (!empty($data['PF_AxisC'])) ? $data['PF_AxisC'] : false;
        $this->PF_BC = (!empty($data['PF_BC'])) ? $data['PF_BC'] : false;
        $this->PF_AG = (!empty($data['PF_AG'])) ? $data['PF_AG'] : false;
        $this->PF_HS = (!empty($data['PF_HS'])) ? $data['PF_HS'] : false;
        $this->PF_SQ = (!empty($data['PF_SQ'])) ? $data['PF_SQ'] : false;
        $this->PF_BRH = (!empty($data['PF_BRH'])) ? $data['PF_BRH'] : false;
        $this->SFlags = (!empty($data['SFlags'])) ? $data['SFlags'] : 0;
        $this->SF_ABS = (!empty($data['SF_ABS'])) ? $data['SF_ABS'] : false;
        $this->SF_TC = (!empty($data['SF_TC'])) ? $data['SF_TC'] : false;
        $this->Tyres = (!empty($data['Tyres'])) ? $data['Tyres'] : '';
        $this->clean = isset($data['clean']) ? $data['clean'] : 0;
        $this->non_clean_reason = (!empty($data['non_clean_reason'])) ? $data['non_clean_reason'] : '';
        $this->time = (!empty($data['time'])) ? $data['time'] : 0;
        $this->time_string = (!empty($data['time_string'])) ? $data['time_string'] : '';
        $this->splits = (!empty($data['splits'])) ? json_decode($data['splits'], true) : '';
        $this->sectors = (!empty($data['sectors'])) ? json_decode($data['sectors'], true) : '';
        $this->added = (!empty($data['added'])) ? $data['added'] : '';
        $this->updated = (!empty($data['updated'])) ? $data['updated'] : '';
        
        $this->PName = (!empty($data['PName'])) ? $data['PName'] : '';
    }

    public function exchangeObject() {
        $data = array();
        $data['id'] = (!empty($this->id)) ? $this->id : 0;
        $data['player_id'] = (!empty($this->player_id)) ? $this->player_id : 0;
        $data['track'] = (!empty($this->track)) ? $this->track : '';
        $data['layout'] = (!empty($this->layout)) ? $this->layout : '';
        $data['car'] = (!empty($this->car)) ? $this->car : '';
        $data['H_Mass'] = (!empty($this->H_Mass)) ? $this->H_Mass : 0;
        $data['H_TRes'] = (!empty($this->H_TRes)) ? $this->H_TRes : 0;
        $data['PFlags'] = (!empty($this->PFlags)) ? $this->PFlags : 0;
        $data['PF_M'] = (!empty($this->PF_M)) ? $this->PF_M : false;
        $data['PF_W'] = (!empty($this->PF_W)) ? $this->PF_W : false;
        $data['PF_KB'] = (!empty($this->PF_KB)) ? $this->PF_KB : false;
        $data['PF_KBS'] = (!empty($this->PF_KBS)) ? $this->PF_KBS : false;
        $data['PF_AC'] = (!empty($this->PF_AC)) ? $this->PF_AC : false;
        $data['PF_AxisC'] = (!empty($this->PF_AxisC)) ? $this->PF_AxisC : false;
        $data['PF_BC'] = (!empty($this->PF_BC)) ? $this->PF_BC : false;
        $data['PF_AG'] = (!empty($this->PF_AG)) ? $this->PF_AG : false;
        $data['PF_HS'] = (!empty($this->PF_HS)) ? $this->PF_HS : false;
        $data['PF_SQ'] = (!empty($this->PF_SQ)) ? $this->PF_SQ : false;
        $data['PF_BRH'] = (!empty($this->PF_BRH)) ? $this->PF_BRH : false;
        $data['SFlags'] = (!empty($this->SFlags)) ? $this->SFlags : 0;
        $data['SF_ABS'] = (!empty($this->SF_ABS)) ? $this->SF_ABS : false;
        $data['SF_TC'] = (!empty($this->SF_TC)) ? $this->SF_TC : false;
        $data['Tyres'] = (!empty($this->Tyres)) ? $this->Tyres : '';
        $data['clean'] = $this->clean ? 1 : 0;
        $data['non_clean_reason'] = (!empty($this->non_clean_reason)) ? $this->non_clean_reason : '';
        $data['time'] = (!empty($this->time)) ? $this->time : 0;
        $data['time_string'] = (!empty($this->time_string)) ? $this->time_string : '';
        $data['splits'] = (!empty($this->splits)) ? json_encode($this->splits) : '';
        $data['sectors'] = (!empty($this->sectors)) ? json_encode($this->sectors) : '';
        $data['added'] = (!empty($this->added)) ? $this->added : date('Y-m-d H:i:s');
        $data['updated'] = (!empty($this->updated)) ? $this->updated : null;
        
        return $data;
    }
    
    public function setPFlags($flags){
        if($flags & PlayerFlags::MOUSE)
            $this->PF_M = true;
        elseif($flags & PlayerFlags::KB_NO_HELP)
            $this->PF_KB = true;
        elseif($flags & PlayerFlags::KB_STABILISED)
            $this->PF_KBS = true;
        else
            $this->PF_W = true;
            
        if($flags & PlayerFlags::AUTOCLUTCH)
            $this->PF_AC = true;
        elseif($flags & PlayerFlags::AXIS_CLUTCH)
            $this->PF_AxisC = true;
        else
            $this->PF_BC = true;
        
        if($flags & PlayerFlags::AUTOGEARS)
            $this->PF_AG = true;
        elseif($flags & PlayerFlags::SHIFTER)
            $this->PF_HS = true;
        else 
            $this->PF_SQ = true;
        
        if($flags & PlayerFlags::HELP_B)
            $this->PF_BRH = true;
    }
    
    public function setSFlags($flags){
        if($flags & SetupFlags::ABS_ENABLE)
            $this->SF_ABS = true;
        if($flags & SetupFlags::TC_ENABLE)
            $this->SF_TC = true;
    }
}