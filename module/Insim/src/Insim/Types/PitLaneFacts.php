<?php

namespace Insim\Types;

class PitLaneFacts {

    const PITLANE_EXIT = 0;         // 0 - left pit lane
    const PITLANE_ENTER = 1;        // 1 - entered pit lane
    const PITLANE_NO_PURPOSE = 2;   // 2 - entered for no purpose
    const PITLANE_DT = 3;           // 3 - entered for drive-through
    const PITLANE_SG = 4;           // 4 - entered for stop-go
    const PITLANE_NUM = 5;

}
