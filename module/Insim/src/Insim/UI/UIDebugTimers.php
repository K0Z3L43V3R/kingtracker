<?php

namespace Insim\UI;

use Insim\Model\PlayerClass;
use Insim\Model\Timer;
use Insim\Packets\isBFN;
use Insim\Packets\isBTC;
use Insim\Packets\isBTN;
use Insim\Packets\isBTT;
use Insim\Types\ButtonStyles;
use Insim\Types\MsgTypes;

class UIDebugTimers extends UI {

    public $columns = array();
    public $rows = array();
    public $cars = array();
    public $filter = array();
    public $search = array();
    public $topActions = array();
    public $own_time = array();
    public $own_buttons = array();
    public $rows_buttons = array();
    public $rows_per_page = 17;
    public $items_per_page = 17;
    public $show_help = false;
    public $trackCode;
    public $tLastDebug = 0;

    function __construct($alias, PlayerClass &$player, $width = 72, $height = 102, $top = 30) {
        $this->width = $width;
        $this->height = $height;
        $this->top = $top;
        $this->left = (200 - $width) / 2;

        // columns
        $this->columns = array(
            'layout' => array('width' => 55, 'name' => 'layout', 'style' => ButtonStyles::ISB_LEFT),
            'load' => array('width' => 15, 'name' => 'load', 'style' => 0),
        );

        // Top actions
        $this->topActions = array();

        parent::__construct($alias, $player);
    }

    public function setData() {

        $this->status_line = '';
    }

    public function show($showBase = true) {
        $this->setData();

        if ($showBase)
            $this->showBase();

        $this->button_content_min = $this->id_current + 1;

        $button = new isBTN();
        $newLine = 0;

        foreach ($this->player->host->hostService->hosts as $host) {
            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L = $this->left + 1;
            $button->T = $this->top + 2 + 12 + $newLine;
            $button->H = 5;
            $button->W = $this->width - 2;
            $button->BStyle = ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY + ButtonStyles::ISB_LEFT;
            $button->Text = MsgTypes::WHITE . "host: ID: {$host->id}, {$host->name} players: " . count(@$host->playerService->players[$host->id]);

            $this->send($button);

            $newLine += 5;

            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->T = $this->top + 2 + 12 + $newLine;
            $button->Text = MsgTypes::WHITE . '  - timers: ' . count($host->timerService->timers);

            $this->send($button);

            $newLine += 5;

            if (count($host->timerService->timers)) {
                foreach ($host->timerService->timers as $timer) {
                    switch ($timer->state) {
                        case Timer::TIMER_RUNNING: $state = 'RUNNING';
                            break;
                        case Timer::TIMER_FINISHED: $state = 'RUNNING';
                            break;
                        default: $state = '';
                            break;
                    }

                    $button->ReqI = ++$this->id_current;
                    $button->ClickID = $button->ReqI;
                    $button->T = $this->top + 2 + 12 + $newLine;
                    $button->Text = MsgTypes::WHITE . "    - {$timer->name} ({$timer->uid}): {$state} {$timer->timer} ";

                    $this->send($button);

                    $newLine += 5;
                }
            }
        }

        $this->showFooter(-2);


        $this->button_content_max = $this->id_current;        
        
        parent::show();
    }

    public function showTopActions() {
        $button = new isBTN();

        $left = $this->left + 3;
        foreach ($this->topActions as $key => $action) {
            $style = isset($action['style']) ? $action['style'] : 0;

            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L = $left;
            $button->T = $this->top + 2 + 6;
            $button->H = 4;
            $button->W = $action['width'];
            $button->BStyle = ButtonStyles::COLOUR_LIGHT_GREY + ButtonStyles::ISB_CLICK + $style;
            $button->Text = $action['title'];
            if (isset($action['typein'])) {
                $button->TypeIn = $action['typein'];
            } else {
                $button->TypeIn = 0;
            }

            $this->send($button);

            $left += $button->W + 1;
            $this->topActions[$key]['id'] = $button->ClickID;
        }
    }

    public function update() {
        if ((time() - $this->tLastDebug) >= 1) {
            $this->redrawContent();

            $this->tLastDebug = time();
        }
    }

    public function redrawContent() {
        $bfn = new isBFN();
        $bfn->SubT = isBFN::BFN_DEL_BTN;
        $bfn->UCID = $this->player->UCID;
        $bfn->ClickID = $this->button_content_min;
        $bfn->ClickMax = $this->button_content_max;
        
        $this->player->host->insim->send($bfn);
        
        $this->id_current = $this->button_content_min;

        $button = new isBTN();
        $newLine = 0;

        foreach ($this->player->host->hostService->hosts as $host) {
            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L = $this->left + 1;
            $button->T = $this->top + 2 + 12 + $newLine;
            $button->H = 5;
            $button->W = $this->width - 2;
            $button->BStyle = ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY + ButtonStyles::ISB_LEFT;
            $button->Text = MsgTypes::WHITE . "host: ID: {$host->id}, {$host->name} players: " . count(@$host->playerService->players[$host->id]);

            $this->send($button);

            $newLine += 5;

            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->T = $this->top + 2 + 12 + $newLine;
            $button->Text = MsgTypes::WHITE . '  - timers: ' . count($host->timerService->timers);

            $this->send($button);

            $newLine += 5;

            if (count($host->timerService->timers)) {
                foreach ($host->timerService->timers as $timer) {
                    switch ($timer->state) {
                        case Timer::TIMER_RUNNING: $state = 'RUNNING';
                            break;
                        case Timer::TIMER_FINISHED: $state = 'RUNNING';
                            break;
                        default: $state = '';
                            break;
                    }

                    $button->ReqI = ++$this->id_current;
                    $button->ClickID = $button->ReqI;
                    $button->T = $this->top + 2 + 12 + $newLine;
                    $button->Text = MsgTypes::WHITE . "    - {$timer->name} ({$timer->uid}): {$state} {$timer->timer} ";

                    $this->send($button);

                    $newLine += 5;
                }
            }
        }

        parent::redrawContent();
    }

    public function eventClick(isBTC $packet) {
        // top action click
        foreach ($this->topActions as $key => $action) {
            if ($action['id'] == $packet->ClickID && !isset($action['typein'])) {
                if (isset($action['callback']))
                    $this->{$action['callback']}();
                return;
            }
        }

        foreach ($this->rows_buttons as $row_id => $buttons) {
            foreach ($buttons as $keyCol => $buttonID) {
                if ($buttonID == $packet->ClickID) {
                    if (!isset($this->rows[$row_id]['layout']) OR $this->rows[$row_id]['load'] == 1)
                        continue;

                    if ($keyCol == 'load') {
                        $this->actionLoadLayout($this->rows[$row_id]['layout']);
                    }
                }
            }
        }

        parent::eventClick($packet);
    }

    public function eventType(isBTT $packet) {
        parent::eventType($packet);
    }

}
