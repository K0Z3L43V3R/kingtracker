<?php

namespace Admin\Controller;

use Admin\Form\AddPubstatForm;
use Admin\Form\AddPubstatInputFilter;
use Auth\Controller\AdminController;
use Exception;
use Insim\Model\PubStat;
use Insim\Service\LFSWService;
use Insim\Service\TaskService;
use Zend\View\Model\ViewModel;

class LFSWController extends AdminController {

    protected $LFSWService;
    protected $taskService;

    public function __construct(LFSWService $LFSWService) {
        $this->LFSWService = $LFSWService;
    }

    public function indexAction() {
        $view = new ViewModel();

        return $view->setVariables(array());
    }

    public function pubstatAction() {
        $view = new ViewModel();

        return $view->setVariables(array(
                    'pubstatKeys' => $this->LFSWService->fetchAll(false)
        ));
    }

    public function addAction() {
        $view = new ViewModel();
        $view->setTemplate('admin/lfsw/add');

        $form = new AddPubstatForm();
        $request = $this->getRequest();

        if ($request->isPost()) {
            $post = $request->getPost();

            $inputFilter = new AddPubstatInputFilter();
            $form->setInputFilter($inputFilter->inputFilter);
            $form->setData($post);
            if ($form->isValid()) {
                $data = $form->getData();

                $pubstat = new PubStat();
                $pubstat->key = $data['key'];
                $this->LFSWService->save($pubstat);
                $this->taskService->addTask(\Insim\Model\Task::TASK_LFSW_UPDATE_PUBSTAT);

                $this->flashMessenger()->setNamespace('success')->addMessage('Item has been added');
                return $this->redirect()->toRoute('admin/default', array('controller' => 'lfsw', 'action' => 'pubstat'));
            }

            $view->setVariable('post', $post);
        }

        return $view->setVariables(array(
                    'form' => $form,
        ));
    }

    public function editAction() {
        $id = intval($this->getEvent()->getRouteMatch()->getParam('id'));

        try {
            $pubstat = $this->LFSWService->getByID($id);
        } catch (Exception $ex) {
            return $this->redirect()->toRoute('admin/default', array('controller' => 'player', 'action' => 'admin'));
        }

        $view = new ViewModel();
        $view->setTemplate('admin/lfsw/add');

        $form = new AddPubstatForm();
        $request = $this->getRequest();

        if ($request->isPost()) {
            $post = $request->getPost();

            $inputFilter = new AddPubstatInputFilter();
            $form->setInputFilter($inputFilter->inputFilter);
            $form->setData($post);
            if ($form->isValid()) {
                $data = $form->getData();

                $pubstat->key = $data['key'];
                $this->LFSWService->save($pubstat);

                $this->taskService->addTask(\Insim\Model\Task::TASK_LFSW_UPDATE_PUBSTAT);
                $this->flashMessenger()->setNamespace('success')->addMessage('Your changes have been saved');
                return $this->redirect()->toRoute('admin/default', array('controller' => 'lfsw', 'action' => 'pubstat'));
            }

            $view->setVariable('post', $post);
        }

        return $view->setVariables(array(
                    'form' => $form,
                    'pubstat' => $pubstat
        ));
    }

    public function deleteAction() {
        $id = intval($this->getEvent()->getRouteMatch()->getParam('id'));

        try {
            $pubstat = $this->LFSWService->getByID($id);
        } catch (Exception $ex) {
            return $this->redirect()->toRoute('admin/default', array('controller' => 'lfsw', 'action' => 'pubstat'));
        }

        $this->LFSWService->delete($pubstat->id);
        $this->taskService->addTask(\Insim\Model\Task::TASK_LFSW_UPDATE_PUBSTAT);

        $this->flashMessenger()->setNamespace('success')->addMessage('Item has been deleted');
        return $this->redirect()->toRoute('admin/default', array('controller' => 'lfsw', 'action' => 'pubstat'));
    }

    public function setTaskService(TaskService $serice) {
        $this->taskService = $serice;
    }

}
