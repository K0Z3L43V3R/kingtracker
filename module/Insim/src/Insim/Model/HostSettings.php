<?php

namespace Insim\Model;

use Application\Model\CoreModel;

class HostSettings extends CoreModel {

    public $id;
    public $host_id;
    public $_group;
    public $name;
    public $value;
    public $data;

    public function exchangeArray($data) {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->host_id = (!empty($data['host_id'])) ? $data['host_id'] : null;
        $this->_group = (!empty($data['_group'])) ? $data['_group'] : null;
        $this->name = (!empty($data['name'])) ? $data['name'] : null;
        $this->value = (!empty($data['value'])) ? $data['value'] : 0;
        $this->data = (!empty($data['data'])) ? json_decode($data['data'], true) : null;
    }

    public function exchangeObject() {
        $data = array();
        $data['id'] = (!empty($this->id)) ? $this->id : null;
        $data['host_id'] = (!empty($this->host_id)) ? $this->host_id : null;
        $data['_group'] = (!empty($this->_group)) ? $this->_group : null;
        $data['name'] = (!empty($this->name)) ? $this->name : null;
        $data['value'] = (isset($this->value)) ? $this->value : 0;
        $data['data'] = (isset($this->data)) ? json_encode($this->data) : null;
        
        return $data;
    }
}
