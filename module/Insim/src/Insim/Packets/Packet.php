<?php

namespace Insim\Packets;

use ReflectionClass;
use ReflectionProperty;

abstract class Packet {
    
    /**
     * Not used
     */
    const ISP_NONE = 0;
    
    /**
     * Insim initialise
     */
    const ISP_ISI = 1; 
    
    /**
     * Version info
     */
    const ISP_VER = 2;
    
    /**
     * Multi purpose
     */
    const ISP_TINY = 3;
    
    /**
     * Multi purpose
     */
    const ISP_SMALL = 4;
    
    const ISP_STA = 5;    //  5 - info            : state info
    const ISP_SCH = 6;    //  6 - inStruction        : single character
    const ISP_SFP = 7;    //  7 - inStruction        : state flags pack
    const ISP_SCC = 8;    //  8 - inStruction        : set car camera
    const ISP_CPP = 9;    //  9 - both ways        : cam pos pack
    const ISP_ISM = 10;   // 10 - info            : start multiplayer
    const ISP_MSO = 11;   // 11 - info            : message out
    const ISP_III = 12;   // 12 - info            : hidden /i message
    const ISP_MST = 13;   // 13 - inStruction        : type message or /command
    const ISP_MTC = 14;   // 14 - inStruction        : message to a connection
    const ISP_MOD = 15;   // 15 - inStruction        : set screen mode
    const ISP_VTN = 16;   // 16 - info            : vote notification
    const ISP_RST = 17;   // 17 - info            : race start
    
    /**
     * New connection
     */
    const ISP_NCN = 18;
    
    /**
     * Connection left
     */
    const ISP_CNL = 19;
    const ISP_CPR = 20;   // 20 - info            : connection renamed
    const ISP_NPL = 21;   // 21 - info            : new player (joined race
    const ISP_PLP = 22;   // 22 - info            : player pit (keeps slot in race
    const ISP_PLL = 23;   // 23 - info            : player leave (spectate - loses slot
    const ISP_LAP = 24;   // 24 - info            : lap time
    const ISP_SPX = 25;   // 25 - info            : split x time
    const ISP_PIT = 26;   // 26 - info            : pit stop start
    const ISP_PSF = 27;   // 27 - info            : pit stop finish
    const ISP_PLA = 28;   // 28 - info            : pit lane enter / leave
    const ISP_CCH = 29;   // 29 - info            : camera changed
    const ISP_PEN = 30;   // 30 - info            : penalty given or cleared
    const ISP_TOC = 31;   // 31 - info            : take over car
    const ISP_FLG = 32;   // 32 - info            : flag (yellow or blue
    const ISP_PFL = 33;   // 33 - info            : player flags (help flags
    const ISP_FIN = 34;   // 34 - info            : finished race
    const ISP_RES = 35;   // 35 - info            : result confirmed
    const ISP_REO = 36;   // 36 - both ways        : reorder (info or inStruction
    const ISP_NLP = 37;   // 37 - info            : node and lap packet
    const ISP_MCI = 38;   // 38 - info            : multi car info
    const ISP_MSX = 39;   // 39 - inStruction        : type message
    const ISP_MSL = 40;   // 40 - inStruction        : message to local computer
    const ISP_CRS = 41;   // 41 - info            : car reset
    const ISP_BFN = 42;   // 42 - both ways        : delete buttons / receive button requests
    const ISP_AXI = 43;   // 43 - info            : autocross layout information
    const ISP_AXO = 44;   // 44 - info            : hit an autocross object
    const ISP_BTN = 45;   // 45 - inStruction        : show a button on local or remote screen
    const ISP_BTC = 46;   // 46 - info            : sent when a user clicks a button
    const ISP_BTT = 47;   // 47 - info            : sent after typing into a button
    const ISP_RIP = 48;   // 48 - both ways        : replay information packet
    const ISP_SSH = 49;   // 49 - both ways        : screenshot
    const ISP_CON = 50;   // 50 - info            : contact (collision report
    const ISP_OBH = 51;   // 51 - info            : contact car + object (collision report
    const ISP_HLV = 52;   // 52 - info            : report incidents that would violate HLVC
    const ISP_PLC = 53;   // 53 - instruction        : player cars
    const ISP_AXM = 54;   // 54 - both ways        : autocross multiple objects
    const ISP_ACR = 55;   // 55 - info            : admin command report
    const ISP_HCP = 56;   // 56 - instruction        : car handicaps
    
    /**
     * New connection - extra info for host
     */
    const ISP_NCI = 57;
    
    const ISP_OCO = 60;

    public function __conStruct($rawPacket = null) {
        if ($rawPacket !== null) {
            $this->unpack($rawPacket);
        }

        return $this;
    }

    /* PHPInSim methods */
    public function &__get($name) {
        return property_exists(get_class($this), $name) ? $this->$name : false;
    }

    public function &__call($name, $arguments) {
        if (property_exists(get_class($this), $name)) {
            $this->$name = array_shift($arguments);
        }

        return $this;
    }

    public function __isset($name) {
        return isset($this->$name);
    }

    public function __unset($name) {
        if (isset($this->$name)) {
            $this->$name = null;
        }
    }

    public function unpack($rawPacket) {
        foreach (unpack($this::UNPACK, $rawPacket) as $property => $value) {
            if (is_string($value)) {
                $value = trim($value);
            }
            $this->$property = $value;
        }

        return $this;
    }

    public function pack() {
        $return = '';
        $packFormat = $this->parsePackFormat();
        $propertyNumber = -1;

        foreach ($this as $property => $value) {
            $pkFnkFormat = $packFormat[++$propertyNumber];

            if ($pkFnkFormat == 'x') {
                $return .= pack('C', 0); # null & 0 are the same thing in Binary (00000000) and Hex (x00), so null == 0.
            } elseif (is_array($pkFnkFormat)) {
                list($type, $elements) = $pkFnkFormat;

                for ($i = 0; $i < $elements; ++$i) {
                    if (isset($value[$i])) {
                        //var_dump($value, $type, $elements, $i, $value[$i]);
                        $return .= pack($type, $value[$i]);
                    } else {
                        $return .= pack("x");
                    }
                }
            } else {
                $return .= pack($pkFnkFormat, $value);
            }
        }

        return $return;
    }

    public function parseUnpackFormat() {
        $return = array();

        foreach (explode('/', $this::UNPACK) as $element) {
            for ($i = 1; is_numeric($element{$i}); ++$i) {
                
            }

            $dataType = substr($element, 0, $i);
            $dataName = substr($element, $i);
            $return[$dataName] = $dataType;
        }

        return $return;
    }

    public function parsePackFormat() {
        $format = $this::PACK; # It does not like using $this::PACK directly.
        $elements = array();

        for ($i = 0, $j = 1, $k = strLen($format); $i < $k; ++$i, ++$j) {    # i = Current Character; j = Look ahead for numbers.
            if (is_string($format{$i}) && !isset($format[$j]) || !is_numeric($format[$j])) {
                $elements[] = $format{$i};
            } else {
                while (isset($format{$j}) && is_numeric($format{$j})) {
                    ++$j;    # Will be the last number of the current element.
                }

                $number = substr($format, $i + 1, $j - ($i + 1));

                if ($format{$i} == 'a' || $format{$i} == 'A') {    # In these cases it's a string type where dealing with.
                    $elements[] = $format{$i} . $number;
                } else { # In these cases, we should get an array.
                    $elements[] = array($format{$i}, $number);
                }

                $i = $j - 1; # Movies the pointer to the end of this element.
            }
        }

        return $elements;
    }

    public static function binaryMask($test, $value){
        return ($value & $test);
    }
    
    public static function getPacketType($value) {
        $class = new ReflectionClass(__CLASS__);
        $constants = array_flip($class->getConstants());


        return isset($constants[$value]) ? $constants[$value] : 'Undefined(' . $value . ')';
    }
}
